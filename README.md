# fill_pdf_utf8

fill_pdf_utf8 fixes PDFtk and Mcpdf unicode issues, so you can write 中国 into your forms. fill_pdf_utf8 aims to become a drop-in replacement for PDFtk.

It is essentially a command line interface to the iText7 PDF and iText5 PDF library with a PDFtk compatible syntax.

## effect picture
![image](http://git.oschina.net/jay_jiang/fill_pdf_utf8/raw/master/example.png)


## Getting Started

Make sure you have installed a Java Runtime Environment such as OpenJDK.

Download the latest standalone version of fill_pdf_utf8 (jar-with-dependencies).

For the sake of brevity, we will refer to this file as fill_pdf_utf8.jar. So you should either rename the file or create a symlink:

ln -s fill_pdf_utf8-1.0.0-jar-with-dependencies.jar fill_pdf_utf8.jar
Test it via:

java -jar fill_pdf_utf8.jar
## Usage

The goal of fill_pdf_utf8 is to become a drop-in replacement for PDFtk, so you could take any working PDFtk command and replace pdftk with java -jar fill_pdf_utf8.jar. That's all there is to it.

Note that not all PDFtk operations are implemented at the moment. Just the following subset is supported.

Please create an issue tracker entry if you see something missing that you need. Don't forget to provide the exact PDFtk command that you would like to see in fill_pdf_utf8.

## Fill in form data

Fill in form data from DATA.xfdf into FORM.pdf and write the result to OUTPUT.pdf:
```
java -jar fill_pdf_utf8.jar template.pdf fontPath ./simfang.ttf fontSize 12.0 fill_form - output - < DATA.xfdf > OUTPUT.pdf
```
Fill in form data and flatten the document to prevent further editing:
```
java -jar fill_pdf_utf8.jar template.pdf fontPath ./simfang.ttf fontSize 12.0 fill_form - output - flatten < DATA.xfdf > OUTPUT.pdf
```
## Add stamp and/or background

Use BACKGROUND.pdf as a background for INPUT.pdf and output to OUTPUT.pdf:
```
java -jar fill_pdf_utf8.jar INPUT.pdf background BACKGROUND.pdf output - > OUTPUT.pdf
```
Similarly, you can use the stamp functionality to add STAMP.pdf as a stamp (i.e. foreground) for INPUT.pdf and output into OUTPUT.pdf. This works just like background, except that it places STAMP.pdf in front of INPUT.pdf instead of behind it:
```
java -jar fill_pdf_utf8.jar INPUT.pdf stamp STAMP.pdf output - > OUTPUT.pdf
```
## Advanced Topics

Combine with Another Library Version

Download the plain JAR file (fill_pdf_utf8-1.0.0.jar) instead of the jar-with-dependencies.

Download the version of the iText PDF library you want to use (itextpdf-X.Y.Z.jar).

Run both in combination:
```
java -cp itextpdf-X.Y.Z.jar:fill_pdf_utf8-1.0.0.jar aero.m_click.fill_pdf_utf8.Main
```
Build from Source

Make sure you have installed Maven and Git.

Download the latest fill_pdf_utf8 source and change into that folder:
```
git clone https://git.oschina.net/jay_jiang/fill_pdf_utf8.git
cd fill_pdf_utf8
```
Build the plain JAR file as well as the jar-with-dependencies:
```
mvn package
```
This will download the correct version of the iText PDF library and all required Maven modules automatically.

You will find the JAR files in the target subfolder.
