package com.stpass.fill_pdf_utf8;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Set;

import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;

public class FillPdfForm {

	private Config config;
	private Map<String, String> paraMap;
	
	public FillPdfForm() {
		// TODO Auto-generated constructor stub
	}
	
	public FillPdfForm(Config config,Map<String, String> paraMap) {
		// TODO Auto-generated constructor stub
		this.config = config;
		this.paraMap = paraMap;
	}
	
	public void xfdfFillPdf() throws IOException {
		execute(config.getPdfInputStream(), config.getPdfOutputStream(), config.getFontPath(), paraMap);
	}
	
	public  void execute(String template,String output,String fontPath,
			Map<String, String> paraMap) throws Exception {
		PdfDocument pdfDoc = new PdfDocument(new PdfReader(template),
				new PdfWriter(output));
		PdfFont pdfFont = PdfFontFactory.createFont(fontPath, PdfEncodings.IDENTITY_H, false);
		generatorFillForm(pdfDoc, pdfFont, paraMap);
	}
	
	
	public void execute(InputStream template, OutputStream output, String fontPath,
			Map<String, String> paraMap) throws IOException {
		PdfDocument pdfDoc = new PdfDocument(new PdfReader(template),
				new PdfWriter(output));
		PdfFont pdfFont = PdfFontFactory.createFont(fontPath, PdfEncodings.IDENTITY_H, false);
		generatorFillForm(pdfDoc, pdfFont, paraMap);
	}
	
	
	public void generatorFillForm(PdfDocument pdfDoc,PdfFont pdfFont,Map<String, String> paraMap) {
		PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDoc, true);
		Set<String> fields = paraMap.keySet();
		Map<String, PdfFormField> map = form.getFormFields();
		for (String key : map.keySet()) {
			PdfFormField field = form.getField(key);
			PdfName type = field.getFormType();
			//批量问题，并且都命名为On的情况 On#0 On#1 On#2 ......
			if (PdfName.Btn.equals(type)) {
				if(fields.contains(key)){
					if ("On".equals(paraMap.get(key))) {
						field.setCheckType(PdfFormField.TYPE_CHECK);
						field.regenerateField();
					} else if ("Off".equals(paraMap.get(key))) {
						field.setCheckType(PdfFormField.TYPE_CROSS);
						field.regenerateField();
					}
				}else {
					if (key.startsWith("On")) {
						if (key.endsWith(".1")) continue; //itext7 bug
						//有默认值时，显示默认的
						if (field.getDefaultValue() == null ) {
							field.setCheckType(PdfFormField.TYPE_CHECK);
							field.regenerateField();
						}
					}else if (key.startsWith("Off")) {
						if (key.endsWith(".1")) continue; //itext7 bug
						field.setCheckType(PdfFormField.TYPE_CROSS);
						field.regenerateField();
					}
				}
			}else {
				if(fields.contains(key)){
					field.setValue(paraMap.get(key), pdfFont, config.getFontSize());
				}
			}
		}
		form.flattenFields();
		pdfDoc.close();
	}

}
