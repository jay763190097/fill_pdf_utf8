package com.stpass.fill_pdf_utf8;

import java.io.InputStream;
import java.io.OutputStream;

public class Config{

    private InputStream pdfInputStream;
    private OutputStream pdfOutputStream;
    private InputStream formInputStream;
    private String fontPath;
    private Float fontSize = 10f;
    private String stampFilename;
    private String backgroundFilename;
    private boolean flatten;

    public InputStream getPdfInputStream() {
        return pdfInputStream;
    }

    public void setPdfInputStream(InputStream pdfInputStream) {
        this.pdfInputStream = pdfInputStream;
    }

    public OutputStream getPdfOutputStream() {
        return pdfOutputStream;
    }

    public void setPdfOutputStream(OutputStream pdfOutputStream) {
        this.pdfOutputStream = pdfOutputStream;
    }

    public InputStream getFormInputStream() {
        return formInputStream;
    }

    public void setFormInputStream(InputStream formInputStream) {
        this.formInputStream = formInputStream;
    }

    public String getFontPath() {
        return fontPath;
    }

    public void setFontPath(String fontPath) {
        this.fontPath = fontPath;
    }

    public String getStampFilename() {
        return stampFilename;
    }

    public void setStampFilename(String stampFilename) {
        this.stampFilename = stampFilename;
    }

    public String getBackgroundFilename() {
        return backgroundFilename;
    }

    public void setBackgroundFilename(String backgroundFilename) {
        this.backgroundFilename = backgroundFilename;
    }

    public boolean isFlatten() {
        return flatten;
    }

    public void setFlatten(boolean flatten) {
        this.flatten = flatten;
    }

    public Float getFontSize() {
        return fontSize;
    }

    public void setFontSize(Float fontSize) {
        this.fontSize = fontSize;
    }
}
