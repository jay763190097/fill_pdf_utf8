package com.itext5.xfdfReader;

import java.util.Map;

/**
 * The handler for the events fired by <CODE>SimpleXMLParser</CODE>.
 * @author Paulo Soares
 */
public interface SimpleXMLDocHandler {
    /**
     * Called when a start tag is found.
     * @param tag the tag name
     * @param h the tag's attributes
     */
    public void startElement(String tag, Map<String, String> h);
    /**
     * Called when an end tag is found.
     * @param tag the tag name
     */
    public void endElement(String tag);
    /**
     * Called when the document starts to be parsed.
     */
    public void startDocument();
    /**
     * Called after the document is parsed.
     */
    public void endDocument();
    /**
     * Called when a text element is found.
     * @param str the text element, probably a fragment.
     */
    public void text(String str);
}
